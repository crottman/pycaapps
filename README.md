# PYCA Apps

This repository has high-level image registration algorithms that use the [PyCA](https://bitbucket.org/scicompanat/pyca) backend.

This contains algorithms for the following:

- Affine Landmark Registration (`SolveAffine()`)
- Affine Intensity Registration (`AffineReg()`)
- Thin Plate Spline Registration (`ThinPlateSplines()`, Bookstein 1989)
- Elastic Image Registration (`ElastReg()`)
- IDiff Image Registration (`IDiff()`, Hinkle 2013)
- Large Deformation Diffeomorphic Metric Mapping (`lddmm()`, Beg 2005)
- Multiscale LDDMM (`lddmmMS()`, Beg 2005)
- Weighted Diffeomorphic Density Registration (`HalfDenMatch()`, Rottman 2015)

## Requirements

This module requires the [PyCA](https://bitbucket.org/scicompanat/pyca) as well as the
[PyCACalebExtras](https://bitbucket.org/crottman/pycacalebextras) module. Both of these must
be in your PYTHONPATH.
