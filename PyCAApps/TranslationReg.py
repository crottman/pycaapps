'''TranslationReg.py - Caleb Rottman

Uses Quasi-Newton algorithm from "Structural and radiometric asymmetry
in brain images" - Joshi, et. al. 2003, but adapted to be translation
only'''

import sys
import PyCA.Core as ca
import PyCA.Common as common

import numpy as np
from numpy.linalg import inv

import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd


def TemplateNCC(Is, It, template_range, search_range):
    '''Given a source and target images (Is, It), and a
    template range [[xmin, xmax], [ymin, ymax]] subsection
    of the source image, and a search range (radius in pixels),
    return the optimal translation.
    '''

    w = cc.SubVol(Is, template_range[0], template_range[1])
    w -= ca.Sum(w)/w.nVox()
    w2 = w.copy()
    ca.Sqr_I(w2)
    sumw2 = ca.Sum(w2)
    f2 = ca.Image3D(w.grid(), w.memType())

    ncc = np.zeros((2*search_range+1, 2*search_range+1))
    # cd.DispImage(w)

    for xi, x in enumerate(xrange(-search_range, search_range+1)):
        for yi, y in enumerate(xrange(-search_range, search_range+1)):
            f = cc.SubVol(It, [template_range[0][0]-x, template_range[0][1]-x],
                          [template_range[1][0]-y, template_range[1][1]-y])

            f.setGrid(w.grid())
            f2.setGrid(w.grid())

            f -= ca.Sum(f)/f.nVox()
            ca.Sqr(f2, f)

            sumf2 = ca.Sum(f2)
            # cd.DispImage(f)
            # sys.exit()

            f *= w
            ncc[xi, yi] = ca.Sum(f)/f.nVox()/np.sqrt(sumw2*sumf2)

    # cd.DispImage(common.ImFromNPArr(ncc), colorbar=True)
    # sys.exit()

    idx = np.array(np.unravel_index(np.argmax(ncc), ncc.shape))
    idx -= np.array([search_range, search_range])

    if abs(idx[0]) == search_range or abs(idx[1]) == search_range:
        print '*Warning, edge case in TemplateNCC*'

    return idx


def TranslationReg(Is, It, A=None, maxIter=50, plot=True):
    '''Translation registration: returns the estimated translation.

    A (optional) is an initial estimate of the affine matrix (action
    matrix, not the inverse)'''
    bg = ca.BACKGROUND_STRATEGY_CLAMP

    Idef = ca.Image3D(It.grid(), It.memType())
    gradIs = ca.Field3D(It.grid(), It.memType())
    gradIdef = ca.Field3D(It.grid(), It.memType())
    Idef_x = ca.Image3D(It.grid(), It.memType())
    Idef_y = ca.Image3D(It.grid(), It.memType())
    h = ca.Field3D(It.grid(), It.memType())
    ca.SetToIdentity(h)
    x = ca.Image3D(It.grid(), It.memType())
    y = ca.Image3D(It.grid(), It.memType())
    diff = ca.Image3D(It.grid(), It.memType())
    scratchI = ca.Image3D(It.grid(), It.memType())

    diffrng = [ca.MinMax(It)[0] - ca.MinMax(Is)[1],
               ca.MinMax(It)[1] - ca.MinMax(Is)[0]]

    is3D = cc.Is3D(Is)
    is2D = not(is3D)

    energy = [[]]

    # convert x, y to world coordinates
    origin = Is.grid().origin().tolist()
    spacing = Is.grid().spacing().tolist()
    ca.Copy(x, h, 0)
    ca.Copy(y, h, 1)
    x *= spacing[0]
    y *= spacing[1]
    x += origin[0]
    y += origin[1]
    if is3D:
        z = ca.Image3D(Is.grid(), Is.memType())
        Idef_z = ca.Image3D(Is.grid(), Is.memType())
        ca.Copy(z, h, 2)
        z *= spacing[2]
        z += origin[2]

    if is2D:
        Vlen = 2
        Adim = 3
    else:
        Vlen = 3
        Adim = 4

    ca.Gradient(gradIs, Is)
    gradIs *= -1

    V = [ca.Image3D(Is.grid(), Is.memType()) for _ in xrange(Vlen)]
    if A is None:
        A = np.identity(Adim)
    A[0:Adim-1, 0:Adim-1] = np.identity(Adim-1)  # make translation only
    Ainv = inv(A)
    a_k = _matToVec(Ainv)

    for it in xrange(maxIter):
        A = inv(Ainv)       # need forward for ApplyAffineReal
        a_k = _matToVec(Ainv)

        cc.ApplyAffineReal(Idef, Is, A, bg)
        ca.Sub(diff, It, Idef)

        cc.ApplyAffineReal(gradIdef, gradIs, A, bg)
        ca.Copy(Idef_x, gradIdef, 0)
        ca.Copy(Idef_y, gradIdef, 1)
        if is3D:
            ca.Copy(Idef_z, gradIdef, 2)

        # Compute V
        if is2D:
            ca.Copy(V[0], Idef_x)
            ca.Copy(V[1], Idef_y)
        else:
            ca.Copy(V[0], Idef_x)
            ca.Copy(V[1], Idef_y)
            ca.Copy(V[2], Idef_z)

        # Compute \int V^T V
        VtV = np.zeros((Vlen, Vlen))
        for i in xrange(Vlen):
            for j in xrange(Vlen):
                if i >= j:  # save computing, VtV is symmetric
                    ca.Mul(scratchI, V[i], V[j])
                    VtV[i, j] = VtV[j, i] = ca.Sum(scratchI)
        # compute diffVt
        diffVt = np.zeros(Vlen)
        for i in xrange(Vlen):
            ca.Mul(scratchI, diff, V[i])
            diffVt[i] = ca.Sum(scratchI)
        # update a_k
        a_k -= np.dot(inv(VtV), diffVt)
        Ainv = _vecToMat(a_k)
        A = inv(Ainv)

        cc.ApplyAffineReal(Idef, Is, A, bg)
        ca.Sub(diff, It, Idef)
        energy[0].append(ca.Sum2(diff))

        if plot:
            # print 'Energy:', energy[0][-1]
            # print A
            cd.DispImage(diff, title='diff', newFig=False, rng=diffrng)

        # print energy[0][it]
        # if it > 20:
        #     print energy[0][it-10] - energy[0][it]
        #     if energy[0][it-10] - energy[0][it]  < energy[0][0] / 10000000.0:
        #         break

    if plot:
        cd.DispImage(Idef)
        cd.EnergyPlot(energy)

    return inv(_vecToMat(a_k))


def _matToVec(Ainv):
    '''Takes a square affine matrix and returns the affine 'vector'
    '''
    if Ainv.shape[0] == 3:         # 2D affine
        a_v = np.zeros(2)
        a_v[0:2] = Ainv[0:2, 2]
    elif Ainv.shape[0] == 4:         # 3D affine
        a_v = np.zeros(3)
        a_v[0:3] = Ainv[0:3, 3]
    else:
        raise Exception("_matToVec: Ainv needs to be 3x3 or 4x4!")
    return a_v


def _vecToMat(a_v):
    '''Takes an affine 'vector' and returns the square affine matrix
    '''
    if a_v.shape[0] == 2:        # 2D affine
        Ainv = np.identity(3)
        Ainv[0:2, 2] = a_v[0:2]
    elif a_v.shape[0] == 3:        # 3D affine
        Ainv = np.identity(4)
        Ainv[0:3, 3] = a_v[0:3]
    else:
        raise Exception("_vecToMat: a_v needs to be length 2 or 3!")
    return Ainv
