* Algorithms Keywords: (lower camelCase)
  - step : float : step size ****
  - sigma : float : regularizer weight ****
  - nIters : uint : number of iterations ****
  - plot : bool : use interactive plots ****
  - verbose : int : 0 = quiet, 1 = 10ish printouts, 2 = verbose

* Deformation algorithms:
  - should return [Idef, h, energy]
  - energy should be [[regularization], [Data Match], [total]]
