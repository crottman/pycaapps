import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import PyCA.Common as common
import numpy as np
# cc.SelectGPU()
plt.ion()
plt.close('all')
import sys
import time
from PrintInfo import printinfo


def IDiffWeighted(I0, I1, penalty, step=.001, sigma=.1, nIters=100, plot=True, verbose=1):
    '''Given two images, register them using a weighted divergence metric

    I0 -- (PyCA Image3D) source image
    I1 -- (PyCA Image3D) target image
    penalty -- weighting to put on the divergence metric.
    step -- (float) step size
    sigma -- (float) deformation regularization weight
    nIters -- (int) number of iterations
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy]
    '''

    mType = I0.memType()
    grid = I0.grid()

    Idef = ca.Image3D(grid, mType)
    diff = ca.Image3D(grid, mType)
    sdetDphi = ca.Image3D(grid, mType)
    detDphi = ca.Image3D(grid, mType)
    scratchI = ca.Image3D(grid, mType)
    scratchF = ca.Field3D(grid, mType)
    phi = ca.Field3D(grid, mType)
    f = ca.Image3D(grid, mType)
    fdef = ca.Image3D(grid, mType)
    v = ca.Field3D(grid, mType)
    gradI1 = ca.Field3D(grid, mType)

    ca.SetToIdentity(phi)
    ca.SetMem(sdetDphi, 1.0)
    ca.SetMem(detDphi, 1.0)
    ca.Copy(f, penalty)
    ca.Abs_I(f)
    ca.Gradient(gradI1, I1)
    ca.SetMem(v, 0.0)

    fluidParams = [1.0, 0.0, 0.0]
    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
    diffOp.setAlpha(fluidParams[0])
    diffOp.setBeta(fluidParams[1])
    diffOp.setGamma(fluidParams[2])
    diffOp.setDivergenceFree(False)
    diffOp.setGrid(grid)
    sizemax = max(grid.size().x, grid.size().y, grid.size().z)

    energy = [[], [], []]

    t = time.time()
    for it in xrange(nIters):
        # background = ca.BACKGROUND_STRATEGY_CLAMP
        bg_im = ca.BACKGROUND_STRATEGY_CLAMP
        bg_field = ca.BACKGROUND_STRATEGY_PARTIAL_ID

        # L2 Image Action
        ca.ApplyH(Idef, I0, phi, bg_im)

        ca.Sub(diff, Idef, I1)
        ca.ApplyH(fdef, f, phi, bg_im)

        # Calculate Energy
        ca.Copy(scratchI, sdetDphi)
        scratchI -= 1.0
        ca.Sqr_I(scratchI)
        scratchI *= fdef
        E1 = sigma * ca.Sum(scratchI)
        E2 = ca.Sum2(diff)
        energy[0].append(E1)
        energy[1].append(E2)
        energy[2].append(E1+E2)
        # if nIters < 10 or it%(nIters/10) == 0 or it == nIters-1:
        #     print 'Iter {0:4d}/{1:4d} : E (w*R + DM) {2:.5e} + {3:.5e} = {4:.5e}' \
        #         .format(it, nIters-1, E1, E2, E1+E2)
        printinfo(it, nIters, energy, verbose=verbose)

        # Calculate Variation
        # term 1 (regularizer)
        ca.Sub(scratchI, sdetDphi, 1.0)
        ca.Neg_I(scratchI)
        scratchI *= fdef
        ca.Gradient(scratchF, scratchI)
        scratchF *= -1.0 * sigma
        ca.Copy(v, scratchF)

        # # term 2
        # ca.Mul(scratchF, gradI1, diff)
        # scratchF *= -2.0
        # v += scratchF
        # # term 3
        # ca.Mul(scratchI, diff, I1)
        # ca.Gradient(scratchF, scratchI)
        # v += scratchF

        # term 2
        ca.Mul(scratchF, gradI1, Idef)
        v -= scratchF
        # term 3
        ca.Gradient(scratchF, Idef)
        scratchF *= I1
        v += scratchF

        mm = ca.MinMax(v)
        if step*max(abs(mm[0]), abs(mm[1])) > sizemax:
            raise OverflowError('v becoming too large after {} iterations!'.format(it))

        # Calculate Update
        diffOp.applyInverseOperator(v)
        v *= step
        ca.SetToIdentity(scratchF)
        ca.Add_I(v, scratchF)   # toH
        ca.ApplyH(scratchF, phi, v, bg_field)
        ca.Copy(phi, scratchF)
        mm = ca.MinMax(phi)
        if max(mm) > sizemax:
            raise OverflowError('phi becoming too large after {} iterations!'.format(it))

        # Update Determinant
        ca.ApplyH(scratchI, detDphi, v, bg_im)
        ca.Copy(detDphi, scratchI)

        ca.SetToIdentity(scratchF)
        ca.Sub_I(v, scratchF)   # toV
        # v *= -1
        ca.Divergence(scratchI, v)
        ca.Exp_I(scratchI)
        detDphi *= scratchI

        # ca.JacDetH(detDphi, phi)
        ca.Sqrt(sdetDphi, detDphi)

    # tt = time.time() - t
    # print "Total Time:", tt
    # print "Iteratins per minute:", nIters/tt*60
    # print "Iteratins per second:", nIters/tt
    # print "secons per iteration:", tt/nIters

    if plot:
        # outdir = 'Results/imweight/'
        # outdir = 'Results/results1/'
        cd.EnergyPlot(energy, legend=['Regularization', 'Data Match', 'Total'])
        ca.Sub(scratchI, I0, I1)
        rng = ca.MinMax(scratchI)
        if cc.Is3D(I0):
            cd.Disp3Pane(scratchI, title='Orig Diff', rng=rng)
            cd.Disp3Pane(diff, title='New Diff', rng=rng)
            cd.Disp3Pane(detDphi, title='Jacobian Determinant', cmap='jet',
                         colorbar=True, bgval=1.0)
        else:
            cd.DispImage(scratchI, title='Orig Diff', rng=rng)
            cd.DispImage(diff, title='New Diff', rng=rng)
            cd.DispImage(detDphi, title='Jacobian Determinant', cmap='jet', colorbar=True)
        cd.DispHGrid(phi, splat=False)

    # return Idef, phi, energy, detDphi
    return Idef, phi, energy


def main():
    grid = cc.MakeGrid((100, 100, 1))
    I0 = ca.Image3D(grid, ca.MEM_DEVICE)
    I1 = ca.Image3D(grid, ca.MEM_DEVICE)
    weight = ca.Image3D(grid, ca.MEM_DEVICE)
    ca.SetMem(I0, 0.0)
    ca.SetMem(I1, 0.0)
    ca.SetMem(weight, 0.0)

    # add appropriate rectangels
    cc.AddRectReal(I0, (10, 15), (40, 40))  # UL
    cc.AddRectReal(I0, (10, 60), (40, 85))  # UR
    cc.AddRectReal(I0, (60, 15), (90, 40))  # LL
    cc.AddRectReal(I0, (60, 60), (90, 85))  # LR
    cc.AddRectReal(I1, (10, 10), (40, 45))
    cc.AddRectReal(I1, (10, 55), (40, 90))
    cc.AddRectReal(I1, (60, 10), (90, 45))
    cc.AddRectReal(I1, (60, 55), (90, 90))
    cc.AddRectReal(weight, (0, 0), (50, 100), 99.9)
    weight += 0.1
    cd.DispImage(I0, title='I0')
    cd.DispImage(I1, title='I1')
    cd.DispImage(weight, title='weight', cmap='jet', colorbar=True)

    IDiffWeighted(I0, I1, weight, step=.1)


if __name__ == '__main__':
    main()
