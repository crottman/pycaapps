'''LDDMM relaxation-based registration, not fully tested'''
import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()

import PyCA.Core as ca

import PyCA.Common as common
import PyCA.Display as display

import numpy as np

import os
import errno
import time

from PrintInfo import printinfo
import PyCACalebExtras.Display as cd


def lddmmMS(I0Orig, I1Orig, scales=None,
            maxPert=None, sigma=None, nIters=None,
            nTimesteps=10, fluidParams=None,
            plot=True, verbose=1):
    '''LDDMM multiscale image registration of two images

    I0Orig -- (PyCA Image3D) source image
    I1Orig -- (PyCA Image3D) target image
    scales -- (list) scale ratios to use (default [2, 1])
    maxPert -- (list) Largest perturbation in a single iteration (default [.2, .02])
    sigma -- (list) deformation regularization weight (default [1.0, 1.0])
    nIters -- (list) number of iterations (default [100, 50])
    nTimesteps -- (int) number of time steps (vector fields), default 10
    fluidParams -- weights of diff operator [grad, div, Id] (default [1, 0, .001])
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy]
    '''

    if fluidParams is None:
        fluidParams = [0.1, 0, 0.001]
    if scales is None:
        scales = [2, 1]
    if nIters is None:
        nIters = [100, 50]
    if sigma is None:
        sigma = [1.0, 1.0]
    if maxPert is None:
        maxPert = [0.2, 0.02]


    mType = I0Orig.memType()
    origGrid = I0Orig.grid()

    # allocate vars
    I0 = ca.Image3D(origGrid, mType)
    I1 = ca.Image3D(origGrid, mType)
    h = ca.Field3D(origGrid, mType)
    diff = ca.Image3D(origGrid, mType)
    gI = ca.Field3D(origGrid, mType)
    gV = ca.Field3D(origGrid, mType)
    scratchI = ca.Image3D(origGrid, mType)
    scratchV = ca.Field3D(origGrid, mType)
    # deformed images
    J = [ca.Image3D(origGrid, mType) for _ in range(nTimesteps)]
    J.insert(0, I0)
    # vector fields
    v = [ca.Field3D(origGrid, mType) for _ in range(nTimesteps)]
    for t in range(nTimesteps):
        ca.SetMem(v[t], 0.0)

    # allocate diffOp
    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()

    # initialize some vars
    scaleManager = ca.MultiscaleManager(origGrid)
    for s in scales:
        scaleManager.addScaleLevel(s)

    # Initalize the thread memory manager (needed for resampler)
    # num pools is 2 (images) + 2*3 (fields)
    ca.ThreadMemoryManager.init(origGrid, mType, 8)

    if mType == ca.MEM_HOST:
        resampler = ca.MultiscaleResamplerGaussCPU(origGrid)
    else:
        resampler = ca.MultiscaleResamplerGaussGPU(origGrid)

    def setScale(scale):
        '''sets the current scale'''
        global curGrid

        scaleManager.set(scale)
        curGrid = scaleManager.getCurGrid()
        # since this is only 2D:
        curGrid.spacing().z = 1.0

        resampler.setScaleLevel(scaleManager)

        diffOp.setAlpha(fluidParams[0])
        diffOp.setBeta(fluidParams[1])
        diffOp.setGamma(fluidParams[2])
        diffOp.setGrid(curGrid)

        # downsample images
        I0.setGrid(curGrid)
        I1.setGrid(curGrid)
        if scaleManager.isLastScale():
            ca.Copy(I0, I0Orig)
            ca.Copy(I1, I1Orig)
        else:
            resampler.downsampleImage(I0, I0Orig)
            resampler.downsampleImage(I1, I1Orig)

        # initialize / upsample deformation
        if scaleManager.isFirstScale():
            for t in range(nTimesteps):
                v[t].setGrid(curGrid)
                ca.SetToZero(v[t])
        else:
            for t in range(nTimesteps):
                resampler.updateVField(v[t])

        # set grids
        gI.setGrid(curGrid)
        diff.setGrid(curGrid)
        gV.setGrid(curGrid)
        scratchI.setGrid(curGrid)
        scratchV.setGrid(curGrid)
        for t in range(nTimesteps):
            J[t+1].setGrid(curGrid)
        h.setGrid(curGrid)

    # end function

    energy = [[] for _ in xrange(3)]

    ustep = None

    for scale in range(len(scales)):

        setScale(scale)
        ustep = None

        tm = time.time()
        for it in range(nIters[scale]):

            # compute deformed images
            ca.SetToIdentity(h)
            for t in range(nTimesteps):
                ca.ComposeHVInv(scratchV, h, v[t])
                h.swap(scratchV)
                ca.ApplyH(J[t+1], I0, h)

            # difference image
            ca.Sub(diff, J[-1], I1)

            # compute deformed images
            ca.SetToIdentity(h)
            for t in reversed(range(nTimesteps)):
                ca.ComposeVInvH(scratchV, v[t], h)
                h.swap(scratchV)
                ca.Gradient(gI, J[t])
                ca.Splat(scratchI, h, diff)
                ca.Mul_I(gI, scratchI)
                diffOp.applyInverseOperator(gV, gI)
                ca.MulC_I(gV, sigma[scale])
                # check maxpert
                ca.Magnitude(scratchI, gV)
                gradmax = ca.Max(scratchI)
                if ustep is None or ustep*gradmax > maxPert:
                    ustep = maxPert[scale]/gradmax
                    print 'step is {}f'.format(ustep)
                # u =  u*(1-2.0*ustep) + (2.0*ustep)*gU
                ca.MulC_Add_MulC_I(v[t], (1-2.0*ustep),
                                   gV, 2.0*ustep)

            gV *= ustep
            # ApplyV(scratchV, h, gV, BACKGROUND_STRATEGY_PARTIAL_ID)
            ca.ComposeHV(scratchV, h, gV)
            h.swap(scratchV)

            # compute energy
            energy[0].append(0)
            energy[1].append(ca.Sum2(diff))
            energy[2].append(ca.Sum2(diff))
            printinfo(it, nIters[scale], energy, verbose=verbose)

            if it == nIters[scale]-1 and plot:
                cd.EnergyPlot(energy)

                plt.figure('results')
                plt.clf()
                plt.subplot(3, 2, 1)
                display.DispImage(I0, 'I0', newFig=False)
                plt.subplot(3, 2, 2)
                display.DispImage(I1, 'I1', newFig=False)
                plt.subplot(3, 2, 3)
                display.DispImage(J[-1], 'def (I0 -> I1)', newFig=False)
                plt.subplot(3, 2, 4)
                display.DispImage(diff, 'diff', newFig=False)
                plt.colorbar()
                # plt.subplot(3,2,5)
                # display.GridPlot(h, every=4, isVF=False)
                # plt.draw()
                plt.show()

        # tt = time.time() - tm
        # print "Total Time:", tt
        # print "Iteratins per minute:", nIters[scale]/tt*60
        # print "Iteratins per second:", nIters[scale]/tt
        # print "secons per iteration:", tt/nIters[scale]

    return (J[-1].copy(), h, energy)
