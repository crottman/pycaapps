import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import numpy as np
import sys
# cc.SelectGPU()
plt.ion()
plt.close('all')


def _sigma(x1, x2, dim):
    '''radial basis function of two points'''
    z = np.linalg.norm(x1-x2)
    if dim == 2:
        eps = 1e-9
        return z**2 * np.log(max(z, eps))
    else:
        return z


def _sigmaF(field, point, dim):
    '''radial basis function of a field and a point'''
    point = ca.Vec3Df(point[0], point[1], point[2])
    Imag = ca.ManagedImage3D(field.grid(), field.memType())  # magnitude
    # scratchF = ca.ManagedField3D(field.grid(), field.memType())
    # ca.SubC(scratchF, field, point)
    field -= point
    ca.Magnitude(Imag, field)
    if dim == 2:
        eps = 1e-9
        scratchI = ca.ManagedImage3D(field.grid(), field.memType())
        cc.ClampImageMin_I(Imag, eps)
        ca.Log(scratchI, Imag)
        ca.MulMul_I(Imag, Imag, scratchI)  # Imag = Imag*Imag*log(Imag)
    field += point                         # add back
    return Imag


def SolveSpline(landmarks):
    ''' given a set of N (2D or 3D) correspondences, i.e. :
    landmarks =     [ [[x0, x1, x2], [y0, y1, y2]],
                 ...
      [[x0, x1, x2], [y0, y1, y2]] ]

    return a dict of the parameters of the radial basis function:

    'params': [ k0, k1, k2 ]    # each is an N-D vector
    'points': [ p0, p1, p2 ]    # each is an N-D vector (copied x values)
    'A': (3x3 matrix)
    't': (lenght 3 vector)

    that describe the deformation field:
    u(x) = sum_I^N k_i phi_i(x) + Ax + t

    such that phi_i(x) =
    ||x_i-x||^2 ln(||x_i-x||) (2D)
    ||x_i-x|| (3D)
    '''

    # convert now to a list of numpy arrays
    landmarks = [(np.array(lm[0]), np.array(lm[1])) for lm in landmarks]

    if len(landmarks[0][0]) == 3:
        dim = 3
        # A = np.array((3, 3))
        # t = np.array(3)
    else:
        dim = 2
        # A = np.array((2, 2))
        # t = np.array(2)

    # affine + translation
    A = np.zeros((dim, dim))
    t = np.zeros(dim)
    N = len(landmarks)

    params = [np.zeros(dim) for lm in landmarks]
    points = [np.array(lm[0]) for lm in landmarks]

    # B matrix (same for each dimension)
    B = np.zeros((N + dim + 1, N + dim + 1))  # linear system to solve
    for i in xrange(N):
        for j in xrange(i+1, N):   # matrix is symmetric w/ 0 diag
            B[i, j] = _sigma(landmarks[i][0], landmarks[j][0], dim)
            B[j, i] = B[i, j]
    for i in xrange(N):
        B[i, N:N+dim+1] = np.append(landmarks[i][0], 1)
        B[N:N+dim+1, i] = B[i, N:N+dim+1]

    # print "final B"
    # print B

    # for each dimension, solve for the parameters separately (lin system: Bx=c)
    c = np.zeros(N+dim+1)
    for k in xrange(dim):
        # construct vector c
        for i in xrange(N):
            c[i] = landmarks[i][1][k]
        # print c

        # Solve linear system
        x = np.linalg.solve(B, c)

        # fill output
        for i in xrange(N):     # k points
            params[i][k] = x[i]
        A[k, :] = x[N:N+dim]
        t[k] = x[-1]

    return dict(A=A, t=t, params=params, points=points)


def DefPoint(p, spline):
    '''given a point p and the spline dictionary, deforms a point as
    defined by the spline
    '''
    p = np.array(p)

    A = spline['A']
    t = spline['t']
    params = spline['params']
    points = spline['points']
    N = len(points)
    dim = len(points[0])

    p_out = np.dot(A, p) + t

    for i in xrange(N):
        p_out += params[i]*_sigma(p, points[i], dim)

    return p_out


def SplineToHField(spline, grid, mType=ca.MEM_DEVICE):
    '''given a PyCA GridInfo object and a spline,
    return the field that describes the transformation

    Note the returned h-field is in Real coordinates
    (because it is assumed that the landmark coordinates
    given are in real coordinates)'''

    A = spline['A']
    t = spline['t']
    params = spline['params']
    points = spline['points']
    N = len(points)
    dim = len(points[0])

    x = ca.ManagedField3D(grid, mType)
    fout = ca.ManagedField3D(grid, mType)

    ca.SetToZero(fout)
    cc.SetToRealIdentity(x)

    # do mem-saving affine (adds 2 big mem pools otherwise)
    cc.MatrixMulMem(fout, A, x)

    if dim == 2:
        fout += ca.Vec3Df(t[0], t[1], 0)
    else:
        fout += ca.Vec3Df(t[0], t[1], t[2])
    fout.toType(mType)
    x.toType(mType)

    # fout is now Ax+t
    for i in xrange(N):
        x.toType(mType)
        rbf = _sigmaF(x, points[i], dim)  # 7 mem fields
        x.toType(ca.MEM_HOST)

        scratchI = ca.ManagedImage3D(grid, mType)
        for j in xrange(3):
            # fout[j] += rbf*k[j]
            ca.Copy(scratchI, fout, j)
            ca.Add_MulC_I(scratchI, rbf, params[i][j])
            ca.Copy(fout, scratchI, j)
        del scratchI
        del rbf

    return fout


def main():
    '''testing code for 2D and 3D'''
    np.random.seed(123456)
    np.set_printoptions(precision=2, linewidth=200, suppress=True)
    pts = [np.array([0, 0, 0]),
           np.array([20, 0, 0]),
           np.array([0, 20, 0]),
           np.array([0, 0, 20]),
           np.array([20, 20, 0]),
           np.array([0, 20, 20]),
           np.array([20, 0, 20]),
           np.array([20, 20, 20]),
           np.array([10, 10, 10])]

    A = np.array([[10, 1.5, 2.5],
                  [4.5, 20, 5.5],
                  [-2.5, -3.5, 30]])
    t = np.array([-15, 5, 25])
    s = 1
    landmarks = [[xi + s*np.random.normal([0, 0, 0]),
                  np.dot(A, xi) + t + s*np.random.normal([0, 0, 0])]
                 for xi in pts]

    d = SolveSpline(landmarks)

    # print 'all landmarks'
    # for p in landmarks:
    #     print p
    print 'A estimated'
    print d['A']
    print 't estimated'
    print d['t']
    print 'k parameters estimated'
    s0 = s1 = s2 = 0.0
    sx0 = sx1 = sx2 = 0.0
    sy0 = sy1 = sy2 = 0.0
    sz0 = sz1 = sz2 = 0.0
    for i, k in enumerate(d['params']):
        print k
        s0 += k[0]
        s1 += k[1]
        s2 += k[2]
        sx0 += k[0]*landmarks[i][0][0]
        sx1 += k[0]*landmarks[i][0][1]
        sx2 += k[0]*landmarks[i][0][2]
        sy0 += k[1]*landmarks[i][0][0]
        sy1 += k[1]*landmarks[i][0][1]
        sy2 += k[1]*landmarks[i][0][2]
        sz0 += k[2]*landmarks[i][0][0]
        sz1 += k[2]*landmarks[i][0][1]
        sz2 += k[2]*landmarks[i][0][2]
    print 'sums', s0, s1, s2
    print 'sums x', sx0, sx1, sx2
    print 'sums y', sy0, sy1, sy2
    print 'sums z', sz0, sz1, sz2

    print '------------------testing points-----------'
    # print landmarks[0][0]
    # DefPoint(landmarks[0][0], d)
    print '| orig x |', "orig x' |", 'transformed |', 'affine only |'
    for lm in landmarks:
        print lm[0], lm[1], DefPoint(lm[0], d), np.dot(A, lm[0]) + t

    print '------------------testing field-----------'
    A = np.array([[1.02, -0.01, 0.05],
                  [.02, 1.01, 0.01],
                  [-.03, -0.04, .98]])
    t = np.array([-3, 3, 1])
    s = 1
    landmarks = [[xi + s*np.random.normal([0, 0, 0]),
                  np.dot(A, xi) + t + s*np.random.normal([0, 0, 0])]
                 for xi in pts]

    d = SolveSpline(landmarks)

    h = SplineToHField(d, cc.MakeGrid([25, 25, 25]))
    # cd.DispHGrid(h)
    # print ca.MinMax(h)

    print '-----------Testing 2D-----------'
    pts = [np.array([10, 10]),
           np.array([90, 10]),
           np.array([10, 90]),
           np.array([90, 90]),
           np.array([45, 45])]

    A = np.array([[1, 0],
                  [0, 1]])
    t = np.array([0, 0])
    s = .1
    landmarks = [[xi + s*np.random.normal([0, 0]),
                  np.dot(A, xi) + t + s*np.random.normal([0, 0])]
                 for xi in pts]

    d = SolveSpline(landmarks)

    # print 'all landmarks'
    # for p in landmarks:
    #     print p
    print 'A estimated'
    print d['A']
    print 't estimated'
    print d['t']
    print 'ks'
    s0 = s1 = 0.0
    sx0 = sx1 = 0.0
    sy0 = sy1 = 0.0
    for i, p in enumerate(d['params']):
        s0 += p[0]
        s1 += p[1]
        sx0 += p[0]*landmarks[i][0][0]
        sx1 += p[0]*landmarks[i][0][1]

        sy0 += p[1]*landmarks[i][0][0]
        sy1 += p[1]*landmarks[i][0][1]
        # print p
    print 'sums', s0, s1
    print 'sums x', sx0, sx1
    print 'sums y', sy0, sy1

    print '------------------testing point-----------'
    print '| orig x |', "orig x' |", 'transformed |', 'affine only |'
    for lm in landmarks:
        print lm[0], lm[1], DefPoint(lm[0], d), np.dot(A, lm[0]) + t

    np.set_printoptions()       # revert

if __name__ == '__main__':
    main()
