'''Elast.py - Caleb Rottman'''
import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCA.Core as ca
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
plt.close('all')
from PrintInfo import printinfo


def ElastReg(Is, It, step=.0005, sigma=.01, nIters=1000,
             fluidParams=None, plot=True, verbose=1):
    '''Elastic registration of two images

    Is -- (PyCA Image3D) source image
    It -- (PyCA Image3D) target image
    step -- (float) step size
    sigma -- (float) deformation regularization weight
    nIters -- (int) number of iterations
    fluidParams -- weights of diff operator [grad, div, Id] (default [1, 0, .001])
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy]
    '''
    # Misc.
    if fluidParams is None:
        fluidParams = [1, 0, 0.001]
    mType = It.memType()
    grid = It.grid()
    scratchI = ca.Image3D(grid, mType)
    scratchF = ca.Field3D(grid, mType)
    diff = ca.Image3D(grid, mType)
    Idef = ca.Image3D(grid, mType)
    gradIs = ca.Field3D(Is.grid(), mType)
    # vec fields
    u = ca.Field3D(grid, mType)
    f = ca.Field3D(grid, mType)
    g = ca.Field3D(grid, mType)
    # allocate diffOp / diffOpParam
    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
    diffOp.setAlpha(fluidParams[0])
    diffOp.setBeta(fluidParams[1])
    diffOp.setGamma(fluidParams[2])
    diffOp.setGrid(grid)

    # initialize variables
    energy = [[] for _ in range(3)]
    ca.SetToZero(u)

    boundary = ca.BC_CLAMP
    ca.Gradient(gradIs, Is, ca.DIFF_CENTRAL, boundary)
    background = ca.BACKGROUND_STRATEGY_CLAMP

    # initial energy
    energy[0].append(0.0)
    ca.SetToIdentity(scratchF)
    print Is.grid()
    print scratchF.grid()
    print scratchI.grid()
    # ca.VtoH_I(scratchF)
    ca.ApplyH(scratchI, Is, scratchF, background)
    ca.HtoV_I(scratchF)

    ca.Sub(diff, scratchI, It)      # Idef - It
    energy[1].append(ca.Sum2(diff))
    energy[2].append(energy[0][-1]+energy[1][-1])
    printinfo(-1, nIters, energy, verbose=verbose)

    for it in xrange(nIters):

        # UPDATE VECTOR FIELDS
        ca.VtoH_I(u)
        ca.ApplyH(Idef, Is, u, background)  # apply Deformations
        # take K * non-laplacian term
        ca.Sub(diff, Idef, It)      # Idef - It
        ca.ApplyH(f, gradIs, u, background)
        ca.HtoV_I(u)

        f *= diff
        diffOp.applyOperator(g, u)

        diffOp.applyInverseOperator(g)
        g *= sigma
        diffOp.applyInverseOperator(f)

        f += g

        # ca.MulC_Add_MulC_I(f, sigma, u, 1)
        # find K * K gradient step from K gradient step
        # diffOp.applyInverseOperator(f)
        # take gradient step
        ca.Add_MulC_I(u, f, -step)
        ca.VtoH_I(u)
        ca.ApplyH(Idef, Is, u, background)  # apply new deformation
        ca.HtoV_I(u)

        printinfo(it, nIters, energy, verbose=verbose)

        # Compute Energy
        diffOp.applyOperator(g, u)
        ca.Magnitude(scratchI, g)
        energy[0].append(ca.Sum2(scratchI)*sigma*.25)  # not sure about .25
        ca.Sub(diff, Idef, It)      # Idef - It
        energy[1].append(ca.Sum2(diff))
        energy[2].append(energy[0][-1]+energy[1][-1])

    if plot:

        cd.EnergyPlot(energy, legend=['Reg', 'Data', 'Total'])

        # cd.DispImage(Idef, title="Elast: Deformed Image")
        # cd.DispImage(It, title="Elast: Target Image")
        # cd.DispImage(Is, title="Elast: Source Image")

        # calculate orig diff
        ca.SetToIdentity(scratchF)
        ca.ApplyH(scratchI, Is, scratchF, background)
        ca.Sub(diff, scratchI, It)      # Is - It
        mm = ca.MinMax(diff)
        cd.DispImage(diff, title="Orig Diff", rng=mm)

        # registered diff
        ca.VtoH_I(u)
        ca.ApplyH(Idef, Is, u, background)  # apply Deformations
        ca.Sub(diff, Idef, It)      # Idef - It
        ca.HtoV_I(u)
        cd.DispImage(diff, title="Reg Diff", rng=mm)

        cd.DispVGrid(u, splat=False)


    ca.VtoH_I(u)
    h = u

    return [Idef, h, energy]


def main():
    '''loads files and runs ElastReg'''
    plt.close('all')
    imagedir = '/home/sci/crottman/GE/DSA/PNG/'

    Is = cc.LoadPNG(imagedir + 'DSAdata000.png')
    It = cc.LoadPNG(imagedir + 'DSAdata100.png')

    ElastReg(Is, It)

if __name__ == '__main__':
    main()
