import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()

import PyCA.Core as ca

# import PyCA.Common as common
# import PyCA.Display as display

import math

import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
from PrintInfo import printinfo


def IDiff(Is, It, step=.05, sigma=.1, nIters=500, plot=True, verbose=1):
    '''Idiff single scale image registration of two images

    Is -- (PyCA Image3D) source image
    It -- (PyCA Image3D) target image
    step -- (float) step size
    sigma -- (float) deformation regularization weight
    nIters -- (int) number of iterations
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy]
    '''
    fluidParams = [1.0, 0.0, 0.0]
    mType = Is.memType()
    grid = Is.grid()
    scratchI = ca.Image3D(grid, mType)
    scratchF = ca.Field3D(grid, mType)
    detDphi = ca.Image3D(grid, mType)
    sdetDphi = ca.Image3D(grid, mType)
    LsdetDphi = ca.Image3D(grid, mType)
    Idef = ca.Image3D(grid, mType)
    Diff = ca.Image3D(grid, mType)
    f = ca.Image3D(grid, mType)
    phi = ca.Field3D(grid, mType)  # H field
    g = ca.Image3D(grid, mType)
    G = ca.Field3D(grid, mType)
    v = ca.Field3D(grid, mType)    # H field
    divf = ca.Field3D(grid, mType)
    divG = ca.Image3D(grid, mType)

    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
        diffOpIncomp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
        diffOpIncomp = ca.FluidKernelFFTGPU()
    diffOp.setAlpha(fluidParams[0])
    diffOp.setBeta(fluidParams[1])
    diffOp.setGamma(fluidParams[2])
    diffOp.setDivergenceFree(False)
    diffOp.setGrid(grid)
    diffOpIncomp.setAlpha(fluidParams[0])
    diffOpIncomp.setBeta(fluidParams[1])
    diffOpIncomp.setGamma(fluidParams[2])
    diffOpIncomp.setDivergenceFree(True)
    diffOpIncomp.setGrid(grid)

    # initialize
    ca.SetToIdentity(phi)
    ca.SetMem(detDphi, 1.0)
    energy = [[] for _ in range(3)]
    Sdiff = 1

    for k in range(nIters):

        if ca.MinMax(phi)[1] > 2.0*max(phi.grid().size().tolist()):
            print 'Eception on iteration', k
            raise Exception('deformation is too large, use smaller step size')

        ca.Sqrt(sdetDphi, detDphi)

        # LsdetDphi => laplacian of sdetDphi
        cc.Laplacian(LsdetDphi, sdetDphi, scratchI)
        ca.Neg_I(LsdetDphi)
        ca.MulC_I(LsdetDphi, 4.0/math.sqrt(128.0))

        # this version of laplacian is not symmetric around 0
        # SetMem(scratchF, 0.0)
        # Copy(scratchF, sdetDphi, 0)
        # diffOp.ApplyOperator(scratchF)
        # Copy(LsdetDphi, scratchF, 0)
        # MulC_I(LsdetDphi, 2.0/math.sqrt(128.0))

        ca.ApplyH(Idef, Is, phi)
        ca.Sub(Diff, Idef, It)

        # Compute energy
        ca.SubC(scratchI, sdetDphi, 1.0)
        energy[0].append(.5*sigma*ca.Sum2(scratchI))
        energy[1].append(ca.Sum2(Diff))
        energy[2].append(energy[0][-1]+energy[1][-1])
        printinfo(k, nIters, energy, verbose=verbose)

        # Gradient
        ca.Gradient(G, Idef)
        ca.Mul_I(G, Diff)

        # div-free part of gradient
        diffOpIncomp.applyInverseOperator(divf, G)
        ca.MulC_I(divf, 2.0)

        ca.Divergence(divG, G) #
        ca.MulC(g, LsdetDphi, 4*sigma)
        ca.Add_I(g, divG)

        ca.SetMem(scratchF, 0.0)
        ca.Copy(scratchF, g, 0)
        diffOp.applyInverseOperator(scratchF)
        ca.Copy(g, scratchF, 0)
        diffOp.applyInverseOperator(scratchF)
        ca.Copy(f, scratchF, 0)

        ca.Gradient(v, f)

        if Sdiff:
            ca.Sub_I(v, divf)
        ca.MulC_I(v, step)
        # toH_I(v)
        ca.SetToIdentity(scratchF)
        ca.Add_I(v, scratchF)
        # compose H Fields
        ca.ApplyH(scratchF, phi, v, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(phi, scratchF)

        # applyH(detDphi, detDphi, v)
        ca.ApplyH(scratchI, detDphi, v, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(detDphi, scratchI)
        ca.MulC(scratchI, g, -step)
        ca.Exp_I(scratchI)
        ca.Mul_I(detDphi, scratchI)

    ca.ApplyH(Idef, Is, phi)
    ca.Sub(Diff, Idef, It)
    # one last energy
    ca.SubC(scratchI, sdetDphi, 1.0)
    energy[0].append(.5*sigma*ca.Sum2(scratchI))  # not sure about the .5
    energy[1].append(ca.Sum2(Diff))
    energy[2].append(energy[0][-1]+energy[1][-1])
    printinfo(nIters, nIters, energy, verbose=verbose)

    if plot:
        cd.EnergyPlot(energy, legend=['detDphi', 'Data', 'Total'], newFig=False)

        cd.DispImage(detDphi, title='detDphi', cmap='jet', colorbar=True)
        print ca.MinMax(detDphi)
        # cd.DispImage(Is, title='Is')
        # cd.DispImage(It, title='It')
        # cd.DispImage(Idef, title='Idef')

        ca.Sub(scratchI, Idef, It)
        rng = [.5*ca.Min(scratchI), .5*ca.Max(scratchI)]
        cd.DispImage(scratchI, title='Reg Diff', rng=rng)
        ca.Sub(scratchI, Is, It)
        cd.DispImage(scratchI, title='Orig Diff', rng=rng)

        # cd.DispHGrid(phi, title='phi-grid')
        cd.DispHGrid(phi, title='phi-grid (inverse)', splat=False)

    return Idef, phi, energy


def main():
    '''test functions w/ hardcoded data :('''
    plt.close('all')
    mType = ca.MEM_DEVICE
    # mType = MEM_HOST

    # # atlaswerks brains
    # imagedir = '/usr/sci/projects/ADNI/data_from_Duygu_9_25_12/'
    # ITKFileIO.LoadImage(imagedir + 'atlas/full_subject_run/MeanImage.mhd', Is)
    # ITKFileIO.LoadImage(imagedir + 'affine_warps/test/AffineTransformedImage1.mhd', It)

    # 2D brains
    # imagedir='/local/crottman/Documents/hereisthematlabcode_/'
    # It = cc.LoadPNG(imagedir + 'DS0002AxialSlice80.png', ds=1)
    # Is = cc.LoadPNG(imagedir + 'DS0003AxialSlice80.png', ds=1)
    # gauss = GaussianFilterGPU()
    # gauss.updateParams(Is.size(), Vec3Df(1.725, 1.725, 1.0), Vec3Di(10))
    # gauss.filter(Is, Is, scratchI)
    # gauss.filter(It, It, scratchI)

    step = .1
    sigma = .1
    nIters = 200

    imagedir = '/home/sci/crottman/GE/DSA/PNG/'
    It = cc.LoadPNG(imagedir + 'DSAdata000.png', ds=2, mType=mType)
    Is = cc.LoadPNG(imagedir + 'DSAdata100.png', ds=2, mType=mType)

    step = .1
    sigma = .01
    nIters = 1000

    IDiff(Is, It, step, sigma, nIters, plot=True)

if __name__ == '__main__':
    main()
