import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import PyCA.Common as common
import numpy as np
import time
import datetime


def printinfo(it, nIters, energy, startTime=None, lastTime=None, verbose=2):
    '''given the current iteration, prints a summary

    Note: energy should be given as [reg_energy, diff_energy, tot_energy]
    where reg_energy is a list or a float.
    '''

    if verbose == 1:
        if nIters <= 10 or it % (nIters/10) == 0 or it >= nIters-1 or it < 0:
            pass
        else:
            return
    elif verbose == 0:
        return

    Initial = it < 0
    Final = it >= nIters
    if Initial or Final:
        it = 0                  # just for spacing

    printstr = 'Iter ' + str(it).rjust(len(str(nIters-1))) + '/' + str(nIters-1)
    if Initial:
        printstr = 'Initial:'.ljust(len(printstr))
    elif Final:
        printstr = 'Final:'.ljust(len(printstr))

    if startTime is not None:
        printstr += ' : {0} :'.format(
            datetime.timedelta(seconds=round(time.time()-startTime)))
    if lastTime is not None:
        printstr += ' : ({0}) '.format(
            datetime.timedelta(seconds=round(time.time()-lastTime)))

    # if energy is a long list of lists ( energy[entype][it] )
    try:
        energy[0][0]
        energy = [en[-1] for en in energy] # now energy is a list of float(s)
    except IndexError:                     # energy value doesn't exist
        print printstr
        return
    except TypeError:
        pass

    if len(energy) == 3:
        printstr += ' E (sigma*Reg + Data): {0:.5e} + {1:.5e} = {2:.5e}'.format(
            energy[0], energy[1], energy[2])
    else:
        printstr += ' E = {0:.5e}'.format(energy[-1])

    print printstr
