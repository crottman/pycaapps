'''LDDMM Single Scale - Caleb Rottman'''
import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()

import PyCA.Core as ca

import PyCA.Common as common
import PyCA.Display as display

import numpy as np

import os
import errno
import time
import sys

from PrintInfo import printinfo
import PyCACalebExtras.Display as cd


def lddmm(I0, I1, maxPert=1.0, sigma=.1, nIters=100, nTimesteps=10,
          fluidParams=None, plot=True, verbose=1):
    '''LDDMM single scale image registration of two images

    I0 -- (PyCA Image3D) source image
    I1 -- (PyCA Image3D) target image
    maxPert -- (float) Largest perturbation in a single iteration
    sigma -- (float) deformation regularization weight
    nIters -- (int) number of iterations
    nTimesteps -- (int) number of time steps (vector fields), default 10
    fluidParams -- weights of diff operator [grad, div, Id] (default [1, 0, .001])
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy]
    '''
    # starttime = time.time()

    if fluidParams is None:
        fluidParams = [1, 0, .001]

    mType = I0.memType()
    grid = I0.grid()
    # print grid

    # Allocate vars
    h = ca.Field3D(grid, mType)
    diff = ca.Image3D(grid, mType)
    gI = ca.Field3D(grid, mType)  # image gradient
    gV = ca.Field3D(grid, mType)  # K grad
    # deformed images
    J = [ca.Image3D(grid, mType) for _ in xrange(nTimesteps)]
    # vec fields
    hlist = [ca.Field3D(grid, mType) for _ in xrange(nTimesteps)]
    scratchI = ca.Image3D(grid, mType)
    scratchF = ca.Field3D(grid, mType)

    for i in xrange(nTimesteps):
        ca.SetToIdentity(hlist[i])

    # allocate diffOp
    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()

    diffOp.setAlpha(fluidParams[0])
    diffOp.setBeta(fluidParams[1])
    diffOp.setGamma(fluidParams[2])
    diffOp.setGrid(grid)

    energy = [[] for _ in xrange(3)]


    ustep = None
    for it in xrange(nIters):

        # calc deformed images (go forward)
        ca.SetToIdentity(h)
        for i in xrange(nTimesteps):
            # h = h(h_i) = h0(h1(...(hi)))
            ca.ComposeHH(scratchF, h, hlist[i])
            h.swap(scratchF)
            ca.ApplyH(J[i], I0, h)

        ca.Sub(diff, J[-1], I1)

        # compute energy
        energy[0].append(0.0)
        energy[1].append(ca.Sum2(diff))  # fix this
        energy[2].append(ca.Sum2(diff))
        printinfo(it, nIters, energy, verbose=verbose)

        # calc/apply derivatives (go backward)
        ca.SetToIdentity(h)
        for i in reversed(xrange(nTimesteps)):
            # splat diff using previous h
            # if i != nTimesteps-1:
            #     ca.Splat(scratchI, h, diff)
            # else:
            #     ca.Copy(scratchI, diff)
            ca.Splat(scratchI, h, diff)

            # print 'diffmoved [-13,7]', ca.MinMax(scratchI)
            # print 'ith image', ca.MinMax(J[i])
            ca.Gradient(gI, J[i])
            gI *= scratchI
            diffOp.applyInverseOperator(gV, gI)

            # we now have the gradient, update h for next time
            ca.ComposeHH(scratchF, hlist[i], h)
            h.swap(scratchF)
            # now update hlist[i]
            ca.HtoV(scratchF, hlist[i])
            ca.Magnitude(scratchI, gV)
            gradmax = ca.Max(scratchI)  # not exact, but close enough
            if ustep is None or ustep*gradmax > maxPert:
                ustep = maxPert/gradmax
                print 'step is:', ustep
            # h -= ustep*(K(splatdiff*gradJ) + vlist[i])
            ca.Add_MulC_I(hlist[i], gV, -ustep)             # diff term
            ca.Add_MulC_I(hlist[i], scratchF, -ustep*sigma)  # reg. term

    # h is now the full deformation
    ca.ApplyH(J[-1], I0, h)

    # calc energy one more time
    ca.SetToIdentity(h)
    for i in xrange(nTimesteps):
        # h = h(h_i) = h0(h1(...(hi)))
        ca.ComposeHH(scratchF, h, hlist[i])
        h.swap(scratchF)
        ca.ApplyH(J[i], I0, h)
    ca.Sub(diff, J[-1], I1)
    energy[0].append(0.0)
    energy[1].append(ca.Sum2(diff))  # fix this
    energy[2].append(ca.Sum2(diff))
    printinfo(nIters, nIters, energy, verbose=verbose)

    if plot:
        mm = ca.MinMax(diff)
        cd.DispImage(diff, title='newdiff', rng=mm)
        ca.Sub(diff, I0, I1)
        cd.DispImage(diff, title='origdiff', rng=mm)
        cd.DispHGrid(h)

    return J[-1], h, energy
