import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import PyCA.Common as common
import numpy as np
# cc.SelectGPU()
plt.ion()
plt.close('all')


def SolveAffine(landmarks, debug=False):
    ''' given a set of N (2D or 3D) real correspondences, i.e. :
    landmarks = [ [[x0, x1, x2], [y0, y1, y2]],
                  ...
                  [[x0, x1, x2], [y0, y1, y2]] ]

    returns an affine transformation where for all landmark pairs,

    A*[x0, x1, x2, 1] \approx [y0, y1, y2, 1]

    If given 3D corresponding points, A is a 4x4 matrix
    If given 2D corresponding points, A is a 3x3 matrix
    '''

    dim = len(landmarks[0][0])
    dim1 = dim+1

    n = len(landmarks)              # number of landmarks
    Msub = np.ones((n, dim1))
    b = np.ones(n*dim)
    for i in xrange(n):
        Msub[i, 0:dim] = landmarks[i][1][:]
        b[i] = landmarks[i][0][0]
        b[i+n] = landmarks[i][0][1]
        if dim > 2:
            b[i+2*n] = landmarks[i][0][2]
    M = np.zeros((n*dim, dim1*dim))
    M[0:n, 0:dim1] = Msub
    M[n:2*n, dim1:2*dim1] = Msub
    if dim > 2:
        M[2*n:3*n, 2*dim1:3*dim1] = Msub

    if n == dim+1:
        z = np.linalg.solve(M, b)
    elif n > dim+1:
        z = np.linalg.lstsq(M, b)
        z = z[0]
    else:
        print "Not enough landmarks!"
        return

    # Note A: BFI -> MRI, Ainv: MRI -> BFI
    Ainv = np.zeros((dim1, dim1))
    Ainv[0][:] = z[0:dim1]
    Ainv[1][:] = z[dim1:2*dim1]
    if dim > 2:
        Ainv[2][:] = z[2*dim1:3*dim1]
    Ainv[dim][dim] = 1
    A = np.linalg.inv(Ainv)

    if debug:
        for lm in landmarks:
            lm0 = list(lm[0])
            lm1 = list(lm[1])
            lm0tolm1 = np.dot(A, lm0 + [1.0])[0:dim]
            lm1tolm0 = np.dot(Ainv, lm1 + [1.0])[0:dim]
            print '[moved lm[0], lm[1]]:', np.around(lm0tolm1, 2), np.around(lm1, 2), \
                'error = ', np.around(np.linalg.norm(lm0tolm1 - lm1), 2)
            print '[lm[0], moved lm[1]]:', np.around(lm0, 2), np.around(lm1tolm0, 2)

    return A


def main():
    '''testing code for 2D and 3D'''
    np.random.seed(123456)
    np.set_printoptions(precision=2, linewidth=200, suppress=True)
    pts = [np.array([0, 0, 0]),
           np.array([20, 0, 0]),
           np.array([0, 20, 0]),
           np.array([0, 0, 20]),
           np.array([20, 20, 0]),
           np.array([0, 20, 20]),
           np.array([20, 0, 20]),
           np.array([20, 20, 20]),
           np.array([10, 10, 10])]

    A1 = np.array([[10, 1.5, 2.5],
                   [4.5, 20, 5.5],
                   [-2.5, -3.5, 30]])
    t = np.array([-15, 5, 25])
    s = .001
    landmarks = [[xi + s*np.random.normal([0, 0, 0]),
                  np.dot(A1, xi) + t + s*np.random.normal([0, 0, 0])]
                 for xi in pts]

    Aest1 = SolveAffine(landmarks, debug=True)

    # print 'all landmarks'
    # for p in landmarks:
    #     print p
    print 'A, t given'
    print A1
    print t
    print 'A estimated'
    print Aest1

    print '-----------Testing 2D-----------'
    pts = [np.array([10, 10]),
           np.array([90, 10]),
           np.array([10, 90]),
           np.array([90, 90]),
           np.array([45, 45])]

    A2 = np.array([[2.2, 0.42],
                   [-0.24, 3.3]])
    t = np.array([4.4, 5.5])
    s = .01
    landmarks = [[xi + s*np.random.normal([0, 0]),
                  np.dot(A2, xi) + t + s*np.random.normal([0, 0])]
                 for xi in pts]

    Aest2 = SolveAffine(landmarks, debug=True)
    print 'A, t given'
    print A2
    print t
    print 'A estimated'
    print Aest2

    print '--------testing AtoH, AtoH2---------'

    A3 = np.array([[1.1, 0, 0, 5],
                   [0, 2.2, 0, 9],
                   [0, 0, 3.3, 14],
                   [0, 0, 0, 1]])

    origin = np.array([-4.0, -9.0, -13.0])
    spacing = np.array([.6, .8, 2.2])
    size = np.array([14, 28, 21])

    h1 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    h2 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    h3 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    # ca.SetMem(h1, 0.0)
    # ca.SetMem(h2, 0.0)
    # ca.SetMem(h3, 0.0)
    cc.SetToRealIdentity(h1)
    cc.SetToRealIdentity(h2)
    cc.SetToRealIdentity(h3)
    print "minmax of h-real"
    print ca.MinMax(h1)


    lowpoint = np.array([0,0,0])*spacing + origin
    highpoint = (size-1.0)*spacing + origin
    lowpoint = np.concatenate((lowpoint, [1]))
    highpoint = np.concatenate((highpoint, [1]))
    print 'lowpoint and highpoint'
    print min(lowpoint)
    print max(highpoint)
    print 'A * lowpoint and highpoint'
    print min(np.dot(A3, lowpoint))
    print max(np.dot(A3, highpoint))
    print 'A-1 * lowpoint and highpoint'
    print min(np.dot(np.linalg.inv(A3), lowpoint))
    print max(np.dot(np.linalg.inv(A3), highpoint))

    cc.AtoH(h1, A3)
    cc.HtoReal(h1)
    cc.AtoHReal(h2, A3)
    # cc.HtoIndex(h2)
    print 'forward minmax'
    print ca.MinMax(h1)
    print ca.MinMax(h2)
    print 'inverse minmax'
    cc.AtoH(h1, np.linalg.inv(A3))
    cc.HtoReal(h1)
    ca.Sub(h3, h1, h2)          # for inverse one
    cc.AtoHReal(h2, np.linalg.inv(A3))

    # cd.DispImage(h1, colorbar=True)
    # cd.DispImage(h2, colorbar=True)
    # cd.DispImage(h3, colorbar=True)
    print ca.MinMax(h1)
    print ca.MinMax(h2)
    print ca.MinMax(h3)

    print '--------testing AtoH, AtoH2 in 2D---------'

    A3 = np.array([[1.1, 0, 5],
                   [0, 2.2, 9],
                   [0, 0, 1]])

    origin = np.array([-4.0, -9.0, 0])
    spacing = np.array([.6, .8, 1])
    size = np.array([14, 28, 1])

    h1 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    h2 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    h3 = ca.Field3D(cc.MakeGrid(size, spacing, origin), ca.MEM_DEVICE)
    # ca.SetMem(h1, 0.0)
    # ca.SetMem(h2, 0.0)
    # ca.SetMem(h3, 0.0)
    cc.SetToRealIdentity(h1)
    cc.SetToRealIdentity(h2)
    cc.SetToRealIdentity(h3)
    print "minmax of h-real"
    print ca.MinMax(h1)

    lowpoint = np.array([0,0,0])*spacing + origin
    lowpoint = lowpoint[0:2]
    highpoint = (size-1.0)*spacing + origin
    highpoint = lowpoint[0:2]
    lowpoint = np.concatenate((lowpoint, [1]))
    highpoint = np.concatenate((highpoint, [1]))
    print 'lowpoint and highpoint'
    print min(lowpoint)
    print max(highpoint)
    print 'A * lowpoint and highpoint'
    print min(np.dot(A3, lowpoint))
    print max(np.dot(A3, highpoint))
    print 'A-1 * lowpoint and highpoint'
    print min(np.dot(np.linalg.inv(A3), lowpoint))
    print max(np.dot(np.linalg.inv(A3), highpoint))

    cc.AtoH(h1, A3)
    cc.HtoReal(h1)
    cc.AtoHReal(h2, A3)
    # cc.HtoIndex(h2)
    print 'forward minmax'
    print ca.MinMax(h1)
    print ca.MinMax(h2)
    print 'inverse minmax'
    cc.AtoH(h1, np.linalg.inv(A3))
    cc.HtoReal(h1)
    ca.Sub(h3, h1, h2)          # for inverse one
    cc.AtoHReal(h2, np.linalg.inv(A3))

    # cd.DispImage(h1, colorbar=True)
    # cd.DispImage(h2, colorbar=True)
    # cd.DispImage(h3, colorbar=True)
    print ca.MinMax(h1)
    print ca.MinMax(h2)
    print ca.MinMax(h3)


    np.set_printoptions()       # revert

if __name__ is '__main__':
    main()
