'''Code for diffeomorphic density matching'''
import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCA.Core as ca
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import sys
# import time
plt.close('all')
from PrintInfo import printinfo


def lin_threshold(Im_out, Im_in, xval, yval):
    ''' for 3 threshold values, we set:
    I(x) < xval[0] => yval[0]
    xval[0] < I(x) < xval[1] => lin(yval[0], yval[1]
    xval[1] < I(x) < xval[2] => lin(yval[1], yval[2]
    xval[2] < I(x)  => yval[3]
    '''
    x = xval
    y = yval
    ca.SetMem(Im_out, 1.0)
    # lower linear
    tmp = Im_in.copy()
    m = (y[1]-y[0])/(x[1]-x[0])
    tmp *= m
    tmp += -m*x[0]+y[0]
    cc.SetRegionLTE(Im_out, Im_in, xval[1], tmp)
    # upper linear
    ca.Copy(tmp, Im_in)
    m = (y[2]-y[1])/(x[2]-x[1])
    tmp *= m
    tmp += -m*x[1]+y[1]
    cc.SetRegionGTE(Im_out, Im_in, xval[1], tmp)
    # lower threshold
    cc.SetRegionLTE(Im_out, Im_in, xval[0], yval[0])
    # upper threshold
    cc.SetRegionGTE(Im_out, Im_in, xval[2], yval[2])


def sig_threshold(Im_out, Im_in, xcent, halfwidth, yval):
    ''' does a sigmoid soft-thesholding of Im_in

    yval[0] is the low threshold, yval[1] is the high value
    xcent is the center of the threshold'''
    k = 1/halfwidth/4
    Imdenom = Im_in.copy()
    L = Im_out.copy()
    ca.SetMem(L, yval[1]-yval[0])
    Imdenom -= xcent
    Imdenom *= -k
    ca.Exp_I(Imdenom)
    Imdenom += 1
    ca.Div(Im_out, L, Imdenom)
    Im_out += yval[0]


def HalfDenMatch(I0, I1, penalty=None, step=.001, sigma=.1, nIters=100,
                 fluidParams=None, plot=True, verbose=1):
    '''Given two half-densities (sqare root of densities), register them
    using a weighted divergence metric

    I0 -- (PyCA Image3D) source (moving) half-density
    I1 -- (PyCA Image3D) target (fixed) half-density
    penalty -- weighting to put on the divergence metric (should have same grid as I1)
        if None, this is just set to 1.0
    step -- (float) step size
    sigma -- (float) deformation regularization weight
    nIters -- (int) number of iterations
    fluidParams -- weights of diff operator [grad, div, Id] (default [1, 0, .001])
    plot -- (bool) if True, does matplotlib plots at the end
    verbose -- (0, 1, 2) verbosity: 0 = no output; 2 = print every iteration

    returns [Deformed Image3D, deformation h-field, energy, Jac. Det. of h (Image3D)]
    '''

    mType = I1.memType()
    grid = I1.grid()

    # returned:
    Idef = ca.Image3D(grid, mType)
    phi = ca.Field3D(grid, mType)
    detDphi = ca.Image3D(grid, mType)

    # Internal
    diff = ca.Image3D(grid, mType)
    sdetDphi = ca.Image3D(grid, mType)
    scratchI = ca.Image3D(grid, mType)
    scratchF = ca.Field3D(grid, mType)
    fdef = ca.Image3D(grid, mType)
    v = ca.Field3D(grid, mType)
    # gradI1 = ca.Field3D(grid, mType)

    ca.SetToIdentity(phi)
    ca.SetMem(sdetDphi, 1.0)
    ca.SetMem(detDphi, 1.0)
    if penalty is None:
        f = ca.Image3D(grid, mType)
        ca.SetMem(f, 1.0)
    else:
        f = penalty
        ca.Abs_I(f)
    # ca.Gradient(gradI1, I1)
    ca.SetMem(v, 0.0)

    if fluidParams is None:
        fluidParams = [1.0, 0.0, 0.0001]
    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()
    diffOp.setAlpha(fluidParams[0])
    diffOp.setBeta(fluidParams[1])
    diffOp.setGamma(fluidParams[2])
    diffOp.setDivergenceFree(False)
    diffOp.setGrid(grid)
    sizemax = max(grid.size().x, grid.size().y, grid.size().z)

    energy = [[], [], []]

    # t = time.time()
    for it in xrange(nIters):
        bg_im = ca.BACKGROUND_STRATEGY_CLAMP

        # apply deformations to the image and f
        if I0.spacing() != I1.spacing() or I0.origin() != I1.origin():
            cc.HtoReal(phi)     # real in I1 spacing
            cc.ApplyHReal(Idef, I0, phi, bg_im)
            cc.ApplyHReal(fdef, f, phi, bg_im)
            cc.HtoIndex(phi)
        else:
            ca.ApplyH(Idef, I0, phi, bg_im)
            ca.ApplyH(fdef, f, phi, bg_im)

        Idef *= sdetDphi        # half density action
        ca.Sub(diff, Idef, I1)
        if it == 0 and plot:
            rng = ca.MinMax(diff)
            if cc.Is3D(diff):
                cd.Disp3Pane(diff, title='Orig Diff', rng=rng)
            else:
                cd.DispImage(diff, title='Orig Diff', colorbar=True, rng=rng)
        # Calculate Energy
        ca.Copy(scratchI, sdetDphi)
        scratchI -= 1.0
        ca.Sqr_I(scratchI)
        scratchI *= fdef
        E1 = sigma * ca.Sum(scratchI)
        E2 = ca.Sum2(diff)
        energy[0].append(E1)
        energy[1].append(E2)
        energy[2].append(E1+E2)
        printinfo(it, nIters, energy, verbose=verbose)

        # Calculate Variation
        # term 1 (regularizer)
        ca.Sub(scratchI, sdetDphi, 1.0)
        ca.Neg_I(scratchI)
        scratchI *= fdef
        ca.Gradient(scratchF, scratchI)
        scratchF *= -1.0 * sigma
        ca.Copy(v, scratchF)

        # term 2
        # ca.Mul(scratchF, gradI1, Idef)
        # calculate gradI1 every time to save mem
        ca.Gradient(scratchF, I1)
        scratchF *= Idef
        v -= scratchF

        # term 3
        ca.Gradient(scratchF, Idef)
        scratchF *= I1
        v += scratchF

        mm = ca.MinMax(v)
        if step*max(abs(mm[0]), abs(mm[1])) > sizemax:
            raise OverflowError('v becoming too large after {} iterations!'.format(it))

        # Calculate Update
        diffOp.applyInverseOperator(v)
        v *= step
        # v *= ca.Vec3Df(1.0, 1.0, 2.0)

        # update phi - apply v to phi
        ca.HtoV_I(phi)          # make phi a v field so we can do smart background
        ca.VtoH_I(v)            # make v a h field so we can composeVH
        ca.ComposeVH(scratchF, phi, v, 1.0, ca.BACKGROUND_STRATEGY_CLAMP)
        ca.Copy(phi, scratchF)  # phi is h field here
        ca.HtoV_I(v)            # v back to a v field

        mm = ca.MinMax(phi)
        if max(abs(mm[0]), abs(mm[1])) > sizemax*2:
            raise OverflowError('phi becoming too large after {} iterations!'.format(it))

        # Update Determinant (with bg condition of 1)
        detDphi -= 1.0
        ca.ApplyV(scratchI, detDphi, v, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
        # ca.ApplyH(scratchI, detDphi, v, ca.BACKGROUND_STRATEGY_CLAMP)
        scratchI += 1.0

        ca.Copy(detDphi, scratchI)

        ca.Divergence(scratchI, v)

        ca.Exp_I(scratchI)
        detDphi *= scratchI

        # ca.JacDetH(detDphi, phi)
        ca.Sqrt(sdetDphi, detDphi)

    # tt = time.time() - t
    # print "Total Time:", tt
    # print "Iteratins per minute:", nIters/tt*60
    # print "Iteratins per second:", nIters/tt
    # print "secons per iteration:", tt/nIters

    if plot:
        # outdir = 'Results/imweight/'
        # outdir = 'Results/results1/'
        cd.EnergyPlot(energy, legend=['Regularization', 'Data Match', 'Total'])
        if cc.Is3D(scratchI):
            cd.Disp3Pane(diff, title='New Diff', rng=rng)
            cd.Disp3Pane(detDphi, title='Jacobian Determinant', cmap='jet',
                         colorbar=True, bgval=1.0)
        else:
            cd.DispImage(diff, title='New Diff', colorbar=True, rng=rng)
            cd.DispImage(detDphi, title='Jacobian Determinant', cmap='jet', colorbar=True)
        cd.DispHGrid(phi, splat=False)

    return Idef, phi, energy, detDphi


def main():
    # I0 = cc.Load('/home/sci/crottman/PNNL/lungdata/Rat-0000-DenNom.mha')
    # I1 = cc.Load('/home/sci/crottman/PNNL/lungdata/Rat-0600-DenNom.mha')
    # ca.Abs_I(I0)
    # ca.Abs_I(I1)

    # test images
    grid = cc.MakeGrid((100, 100, 1))
    mType = ca.MEM_DEVICE
    I0 = ca.Image3D(grid, mType)
    I1 = ca.Image3D(grid, mType)
    scratch1 = ca.Image3D(grid, mType)
    scratch2 = ca.Image3D(grid, mType)

    ca.SetMem(I0, 0.0)
    ca.SetMem(I1, 0.0)
    cc.AddRect(I0, (25, 25), (75, 50), 1.0)
    cc.AddRect(I0, (25, 50), (75, 75), 5.0)
    cc.AddRect(I1, (25, 25), (75, 40), 5.0/3.0)
    cc.AddRect(I1, (25, 40), (75, 65), 5.0)

    g = ca.GaussianFilterGPU()
    g.updateParams(grid.size(), ca.Vec3Df(.7, .7, 0.0), ca.Vec3Di(2, 2, 1))
    g.filter(scratch1, I0, scratch2)
    ca.Copy(I0, scratch1)
    g.filter(scratch1, I1, scratch2)
    ca.Copy(I1, scratch1)
    print 'full density'
    print ca.Sum(I0)
    print ca.Sum(I1)
    ca.Sqrt_I(I0)
    ca.Sqrt_I(I1)
    print 'half density'
    print ca.Sum(I0)
    print ca.Sum(I1)

    # cd.DispImage(I0)
    # cd.DispImage(I1)
    # sys.exit()

    # HalfDenMatch(I0, I1, I0, nIters=500, step=.5, sigma=.1)
    phi, Idef, energy, detDphi \
        = HalfDenMatch(I0, I1, None, nIters=500, step=.01, sigma=.1)

    print 'half density deformed'
    print ca.Sum(Idef)
    print ca.Sum(I1)
    ca.Sqr_I(Idef)
    ca.Sqr_I(I1)
    print 'full density deformed'
    print ca.Sum(Idef)
    print ca.Sum(I1)
    # HalfDenMatch(I0, I1, 10, step=.01)

if __name__ is '__main__':
    main()
